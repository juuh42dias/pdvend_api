Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :products
      post :freight_calculations, to: 'freight_calculations#create'
    end
  end
end
