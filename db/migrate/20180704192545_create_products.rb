class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.decimal :value
      t.integer :height
      t.integer :width
      t.float :weight
      t.integer :product_length

      t.timestamps
    end
  end
end
