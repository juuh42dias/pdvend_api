# PDVend API

## Welcome to guideline

## Requirements
* Ruby 2.5.1

You can use to install Ruby, RVM, RBenv or ASDF.

Install using: [RVM](https://rvm.io/rvm/install),
Install using: [RBenv](https://github.com/rbenv/rbenv#installation),
Install using: [ASDF](https://github.com/asdf-vm/asdf-ruby#install),

### Step by step to instalation:
* `$ git clone git@bitbucket.org:juuh42dias/pdvend_api.git `
Download Bitbucket code project

## Creating the database:
* `$ sudo -u postgres psql;`
* `$ create role pdvend_api with createdb login password '123456';`
* `$ rails db:create`

### Step to setup project
* `$ bundle install`
* `$ rails db:setup`

### Command to run project
* `$ rails server`
### Command to run tests
* `$ rspec`

### API documentation
Open project folder:
[pdvend_api/doc/api](pdvend_api/doc/api/index.html)
or open [`http://localhost:3000/api/docs`](http://localhost:3000/api/docs) //ApiToMe
---

## App routes
```
GET    /api/v1/products(.:format)                                                               api/v1/products#index
POST   /api/v1/products(.:format)                                                               api/v1/products#create
GET    /api/v1/products/:id(.:format)                                                           api/v1/products#show
PATCH  /api/v1/products/:id(.:format)                                                           api/v1/products#update
PUT    /api/v1/products/:id(.:format)                                                           api/v1/products#update
DELETE /api/v1/products/:id(.:format)                                                           api/v1/products#destroy
POST   /api/v1/freight_calculations(.:format)                                                   api/v1/freight_calculations#create
```
