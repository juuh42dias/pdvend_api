# frozen_string_literal: true

require 'acceptance_helper'

resource 'Products' do
  header 'Accept', 'application/json'
  header 'Content-Type', 'application/json'

  let(:product) { FactoryBot.create(:product) }

  get '/api/v1/products' do
    parameter :page, 'Current page of products'

    let(:page) { 1 }

    before do
      FactoryBot.create_list(:product, 2)
    end

    example_request 'Getting a list of products' do
      expect(response_body).to eq(Product.all.to_json)
      expect(status).to eq(200)
    end
  end

  head '/api/v1/products' do
    example_request 'Getting the headers' do
      expect(response_headers['Cache-Control']).to eq('max-age=0, private, must-revalidate')
    end
  end

  post '/api/v1/products' do
    parameter :name, 'Product name', required: true
    parameter :description, 'Product description'
    parameter :value, 'Product value', required: true
    parameter :height, 'Product height', required: true
    parameter :width, 'Product width', required: true
    parameter :weight, 'Product weight', required: true
    parameter :product_length, 'Product length', required: true

    response_field :name, 'Product name', 'Type' => 'String'
    response_field :description, 'Product description', 'Type' => 'Text'
    response_field :value, 'Product value', 'Type' => 'Decimal'
    response_field :height, 'Product height', 'Type' => 'Integer'
    response_field :width, 'Product width', 'Type' => 'Integer'
    response_field :weight, 'Product weight', 'Type' => 'Float'
    response_field :product_length, 'Product length', 'Type' => 'Integer'

    let(:name) { product.name }
    let(:description) { product.description }
    let(:value) { product.value }
    let(:height) { product.height }
    let(:width) { product.width }
    let(:weight) { product.weight }
    let(:product_length) { product.product_length }

    let(:raw_post) { params.to_json }

    example_request 'Creating a Product' do
      explanation 'First, create a Product, then make a later request to get it back'

      @product = JSON.parse(response_body)
      expect(@product.except('id', 'created_at', 'updated_at')).to eq(
        'name' => name,
        'description' => description,
        'value' => value.to_s,
        'height' => height,
        'width' => width,
        'weight' => weight,
        'product_length' => product_length
      )
      expect(status).to eq(201)
    end
  end

  get '/api/v1/products/:id' do
    let(:id) { product.id }

    example_request 'Getting a specific Product' do
      expect(response_body).to eq(product.to_json)
      expect(status).to eq(200)
    end
  end

  put '/api/v1/products/:id' do
    parameter :name, 'Product name'
    parameter :description, 'Product description'
    parameter :value, 'Product value'
    parameter :height, 'Product height'
    parameter :width, 'Product width'
    parameter :weight, 'Product weight'
    parameter :product_length, 'Product length'

    let(:id) { product.id }
    let(:description) { 'Description of new product' }
    let(:value) { 50.20 }
    let(:height) { 3 }
    let(:width) { 7 }
    let(:weight) { 1.0 }
    let(:product_length) { 2 }

    let(:raw_post) { params.to_json }

    example_request 'Updating a Product' do
      expect(status).to eq(200)
    end
  end

  delete '/api/v1/products/:id' do
    let(:id) { product.id }

    example_request 'Deleting a Product' do
      expect(status).to eq(204)
    end
  end
end
