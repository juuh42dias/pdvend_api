# frozen_string_literal: true

require 'acceptance_helper'

resource 'FreightCalculations' do
  header 'Accept', 'application/json'
  header 'Content-Type', 'application/json'

  let!(:product) do
    FactoryBot.create(:product,
                      weight: 0.3,
                      product_length: 30,
                      width: 15,
                      height: 2)
  end

  post '/api/v1/freight_calculations' do
    parameter :product_id, 'Product product_id'
    parameter :zip_to, 'Product zip_to'

    response_field :id, 'Product id', scope: :product, 'Type' => 'Integer'
    response_field :name, 'Product name ', scope: :product, 'Type' => 'String'
    response_field :description, 'Product description', scope: :product, 'Type' => 'String'
    response_field :value, 'Product value', scope: :product, 'Type' => 'Float'
    response_field :height, 'Product height', scope: :product, 'Type' => 'Integer'
    response_field :width, 'Product width', scope: :product, 'Type' => 'Integer'
    response_field :weight, 'Product weight', scope: :product, 'Type' => 'Float'
    response_field :product_length, 'Product length', scope: :product, 'Type' => 'Integer'
    response_field :created_at, 'Product timestamp', scope: :product, 'Type' => 'Timestamp'
    response_field :updated_at, 'Product timestamp', scope: :product, 'Type' => 'Timestamp'

    response_field :freight, 'Freight hash', 'Type' => 'Hash'

    response_field :sedex, 'Sedex hash', scope: [:freight], 'Type' => 'Hash'
    response_field :codigo, 'Freight Sedex code', scope: [:feight, :sedex], 'Type' => 'String'
    response_field :valor, 'Freight Sedex value', scope: [:freight, :sedex], 'Type' => 'Float'
    response_field :prazo_entrega, 'Freight Sedex deadline', scope: :freight, 'Type' => 'Integer'
    response_field :valor_mao_propria, 'Freight Sedex own hand', scope: [:freight, :sedex], 'Type' => 'Float'
    response_field :valor_aviso_recebimento, 'Freight Sedex acknowledgment value', scope: [:freight, :sedex], 'Type' => 'Float'
    response_field :valor_valor_declarado, 'Freight Sedex declared value', scope: [:freight, :sedex], 'Type' => 'Float'
    response_field :entrega_domiciliar, 'Freight Sedex home delivery', scope: [:freight, :sedex], 'Type' => 'Boolean'
    response_field :entrega_sabado, 'Freight Sedex Saturday delivery', scope: [:freight, :sedex], 'Type' => 'Boolean'
    response_field :erro, 'Freight Sedex error', scope: [:freight, :sedex], 'Type' => 'Integer'
    response_field :tipo, 'Freight Sedex type', scope: [:freight, :sedex], 'Type' => 'String'
    response_field :nome, 'Freight Sedex name', scope: [:freight, :sedex], 'Type' => 'String'
    response_field :descricao, 'Freight Sedex description', scope: [:freight, :sedex], 'Type' => 'String'

    response_field :pac, 'Pac hash', scope: [:freight], 'Type' => 'Hash'
    response_field :codigo, 'Freight PAC code', scope: [:feight, :pac], 'Type' => 'String'
    response_field :valor, 'Freight PAC value', scope: [:freight, :pac], 'Type' => 'Float'
    response_field :prazo_entrega, 'Freight PAC deadline', scope: :freight, 'Type' => 'Integer'
    response_field :valor_mao_propria, 'Freight PAC own hand', scope: [:freight, :pac], 'Type' => 'Float'
    response_field :valor_aviso_recebimento, 'Freight PAC acknowledgment value', scope: [:freight, :pac], 'Type' => 'Float'
    response_field :valor_valor_declarado, 'Freight PAC declared value', scope: [:freight, :pac], 'Type' => 'Float'
    response_field :entrega_domiciliar, 'Freight PAC home delivery', scope: [:freight, :pac], 'Type' => 'Boolean'
    response_field :entrega_sabado, 'Freight PAC Saturday delivery', scope: [:freight, :pac], 'Type' => 'Boolean'
    response_field :erro, 'Freight PAC error', scope: [:freight, :pac], 'Type' => 'Integer'
    response_field :tipo, 'Freight PAC type', scope: [:freight, :pac], 'Type' => 'String'
    response_field :nome, 'Freight PAC name', scope: [:freight, :pac], 'Type' => 'String'
    response_field :descricao, 'Freight PAC description', scope: [:freight, :pac], 'Type' => 'String'

    response_field :status, 'Response status', 'Type' => 'String'

    let(:product_id) { product.id }
    let(:zip_to) { '75690000' }

    let(:raw_post) { params.to_json }

    example_request 'Creating a Freight Calculation' do
      explanation 'First, create a Freight Calculation, then make a later request to get it back'

      @product = JSON.parse(response_body)
      expect(@product['product']).to include(
        'id' => product.id
      )
      expect(@product['freight']['sedex']).to include(
        'erro' => '0'
      )
      expect(@product['freight']['pac']).to include(
        'erro' => '0'
      )
      expect(@product).to include('status' => 'ok')
    end
  end

  post '/api/v1/freight_calculations' do
    parameter :product_id, 'Product product_id'
    parameter :zip_to, 'Product zip_to'

    response_field :error, 'Freight calculation error', 'Type' => 'Integer'
    response_field :status, 'Freight calculation status', 'Type' => 'String'

    let(:product_id) { product.id }
    let(:zip_to) { '756900' }

    let(:raw_post) { params.to_json }

    example_request 'Freight Calculation with invalid params' do
      explanation 'First, create a Freight Calculation, then make a later request to get it back'

      @product = JSON.parse(response_body)
      expect(@product).to match(
        'error' => 'CEP informado inválido e ou ausente', 'status' => 401
      )
    end
  end
end
