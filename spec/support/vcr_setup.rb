# frozen_string_literal: true

VCR.configure do |config|
  config.default_cassette_options = { record: :new_episodes, erb: true }

  config.cassette_library_dir = 'fixtures/vcr_cassettes'
  config.hook_into :webmock
  config.allow_http_connections_when_no_cassette = true
end
