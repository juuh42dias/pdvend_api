# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    name { FFaker::Product.product_name }
    description { FFaker::Product.product }
    value { [3.50, 45.10, 85.00, 99.99].sample }
    height { rand(99) }
    width { rand(99) }
    weight { [1.5, 2.0, 5.15, 6.10].sample }
    product_length { rand(99) }
  end
end
