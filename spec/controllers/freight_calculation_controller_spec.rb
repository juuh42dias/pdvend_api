# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::FreightCalculationsController do
  let(:product) { FactoryBot.create(:product) }
  let(:params) do
    {
      product_id: product.id,
      zip_to: '75690000'
    }
  end

  describe 'GET #create' do
    context 'when param is valid' do
      before do
        VCR.use_cassette('correios-frete-ok') do
          post :create, params: params
        end
      end

      it 'creates a freight' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when param is not valid' do
      it 'returns error with product_id invalid' do
        VCR.use_cassette('correios-frete-product_id_invalid') do
          post :create, params: { product_id: 0, zip_to: '75690000' }
        end
        expect(JSON.parse(response.body)).to match('error' => I18n.t('product.incorrect'), 'status' => 401)
      end

      it 'returns error with product_id is blank' do
        VCR.use_cassette('correios-frete-product_id_blank') do
          post :create, params: { product_id: nil, zip_to: '75690000' }
        end
        expect(JSON.parse(response.body)).to match('error' => I18n.t('product.blank'), 'status' => 401)
      end

      it 'returns error with zip code is invalid' do
        VCR.use_cassette('correios-frete-zip_to_invalid') do
          post :create, params: { product_id: product.id, zip_to: '7569' }
        end
        expect(JSON.parse(response.body)).to match('error' => I18n.t('freight.zip_to.error'), 'status' => 401)
      end

      it 'returns error with product_id invalid' do
        VCR.use_cassette('correios-frete-msg_erro') do
          post :create, params: { product_id: product.id, zip_to: '75690' }
        end
        expect(JSON.parse(response.body)).to match('error' => 'CEP informado inválido e ou ausente', 'status' => 401)
      end
    end
  end
end
