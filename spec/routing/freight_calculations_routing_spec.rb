# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::FreightCalculationsController, type: :routing do
  describe 'routing' do
    it 'routes to #create' do
      expect(post: api_v1_freight_calculations_path).to route_to('api/v1/freight_calculations#create')
    end
  end
end
