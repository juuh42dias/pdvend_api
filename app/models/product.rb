# frozen_string_literal: true

class Product < ApplicationRecord
  validates :name, :height, :width, :weight, :product_length, presence: true
end
