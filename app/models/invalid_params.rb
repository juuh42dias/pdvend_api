# frozen_string_literal: true

class InvalidParams < StandardError
  attr_accessor :message, :status
  def initialize(message, status = 500)
    @message = message
    @status = status
  end
end
