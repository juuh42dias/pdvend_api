# frozen_string_literal: true

module Api
  module V1
    class FreightCalculationsController < ApplicationController
      before_action :validate_params, only: :create

      def create
        @product ||= product
        @freight ||= freight(@product)
        render json: { product: @product, freight: @freight, status: :ok } unless freight_error?(@freight)
      end

      private

      def freight(product)
        freight = Correios::Frete::Calculador.new(cep_origem: ZIP_FROM,
                                                  cep_destino: params[:zip_to].presence,
                                                  peso: product.weight,
                                                  comprimento: product.product_length,
                                                  largura: product.width,
                                                  altura: product.height)
        freight.calculate :sedex, :pac
      end

      def product
        Product.find(params[:product_id].presence)
      rescue StandardError
        raise InvalidParams.new(I18n.t('product.incorrect'), 401)
      end

      def validate_params
        raise InvalidParams.new(I18n.t('product.blank'), 401) unless params[:product_id].present?
        raise InvalidParams.new(I18n.t('freight.zip_to.error'), 401) unless params[:zip_to] =~ /\d{8}/
      end

      def freight_error?(freight)
        return false unless freight[:pac].error? || freight[:sedex].error?
        error_message = freight[:sedex].msg_erro || freight[:pac].msg_erro
        raise InvalidParams.new(error_message, 401)
      end
    end
  end
end
