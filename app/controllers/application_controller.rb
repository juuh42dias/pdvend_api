# frozen_string_literal: true

class ApplicationController < ActionController::API
  rescue_from InvalidParams do |error|
    render json: { error: error.message, status: error.status }
  end
end
